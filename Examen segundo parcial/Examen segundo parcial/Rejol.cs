﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen_segundo_parcial
{
    class Reloj
    {
        private static Reloj reloj;
        private Reloj()
        { 
        }


       //constructor privado por que se usa el patron Singleton
        

        private static void createinstance()
        {
            if (reloj == null)
            {
                reloj = new Reloj();
                reloj.run();
            }
            return ;

        }

        // Obtiene la unica instancia 

        public static Reloj getInstancia()
        {

            createinstance();
            return reloj;
        }

        //Imprime por pantalla la hora cada segundo
         


        public void run()
        {
        
            int hora = 00;
            int minuto = 00;
            int segundo = 00;
            int x;
            while (true)
            {
                Console.WriteLine("La hora es  {0}:{1}:{2}", hora, minuto, segundo);

                x = Environment.TickCount;

                while (Environment.TickCount <= x + 1000) ;
                segundo = segundo + 1;

                if (segundo == 60)
                {
                    segundo = 0;
                    minuto = minuto + 1;
                    if (minuto == 60)
                    {
                        minuto = 0;
                        hora = hora + 1;
                        if (hora == 24)
                        {
                            hora = 0;

                        }
                    }
                }
                Console.ReadKey();

            }

        }
    }
}

