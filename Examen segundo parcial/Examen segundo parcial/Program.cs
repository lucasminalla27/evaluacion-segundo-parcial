﻿using System;

namespace Examen_segundo_parcial
{
    class Program
    {
        static void Main(string[] args)
        {
            // 3 relojes usando el patrón singleton   
            Reloj r = Reloj.getInstancia();
            Reloj r2 = Reloj.getInstancia();
            Reloj r3 = Reloj.getInstancia();
        }
    }
}
